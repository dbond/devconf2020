Use [MyOldBankingDB];

--Account
If Exists (Select 1 From sys.triggers Where name = 'ti_dbo_Account_CT')
Begin
	Drop TRIGGER [cdc].[ti_dbo_Account_CT]
End
GO

exec [dbo].[Build_cdc_trigger_perTable] @schema='cdc', @Table='dbo_Account_CT'
