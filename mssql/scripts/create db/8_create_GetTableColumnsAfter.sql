USE [MyOldBankingDB]
GO

CREATE FUNCTION [dbo].[GetTableColumnsAfter] (
    @TableName Varchar(200)
)
RETURNS varchar(max)
AS
Begin

    Declare @ListColumns nvarchar(max)

    --Get the column hearders from the system table
    SELECT 
		@ListColumns = coalesce(@ListColumns + CHAR(13) +'''", "', ''' "') + 
		convert(varchar(200),COLUMN_NAME)+'":"''' + '	+ isnull(cast(a.'+  convert(varchar(200),COLUMN_NAME)+' as nvarchar(max)),'''')  + '
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = @TableName
    and ORDINAL_POSITION > 5
    ORDER BY ORDINAL_POSITION

    RETURN @ListColumns

End