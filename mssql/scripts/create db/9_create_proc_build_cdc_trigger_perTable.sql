USE [MyOldBankingDB]
GO
 
Create procedure [dbo].[Build_cdc_trigger_perTable]
    @Schema varchar(20), 
    @Table varchar(200)
as 

Begin Try
 
Declare @sql nvarchar(max)

Select   @sql = 
'Create TRIGGER [cdc].[ti_' + table_name +']
       ON  [MyOldBankingDB].[cdc].[' + table_name +']
       AFTER  INSERT
    AS 

/****** Automated Script Creation Date: ' + convert(varchar,Getdate(),120) + ' ******/

set nocount on 
    

    Insert Into [MyOldBankingDB].[cdc].[cdc_change_data]
    Select 
        [__$start_lsn]								as LSN, 
        [__$seqval]									as LSNSV , 
        getdate()								    as ChangeDate,
        '''+ db_name() +'''										as SourceDatabase, 
        '''+ LEFT(table_name, CHARINDEX('_', table_name) - 1) +'''				as SourceSchema,  
        '''+ replace(replace(replace(table_name,'projectionDb_',''),'dbo_',''),'_CT','')+'''	as SourceTable , 
        case [__$operation] 
            when 1 then ''D''
            when 2 then ''I''
        ELSE 
            ''E'' 
        end											as Operation, 
        0 as IsProcessing,
        case [__$operation] 
            when 2 then 
            ''{'' +
            '' "Data": {'' +
            '+ 	 [dbo].[GetTableColumns](table_name) +'
            ''"}'' +
                ''}'' 
            when 1 then 
            ''{'' +
            '' "Data": {'' + 
            '+ 	 [dbo].[GetAllKeyColumns](table_name) +'
        ''"}'' +
        ''}'' 
        Else 
            ''Error''	
        end as json
    FROM [inserted]
        where [__$operation] in (1, 2)
		
	Union 
    
    Select 
        a.[__$start_lsn]							as LSN, 
        a.[__$seqval]								as LSNSV , 
        getdate()								    as ChangeDate,
        '''+ db_name() +'''										as SourceDatabase, 
        '''+ LEFT(table_name, CHARINDEX('_', table_name) - 1) +'''				as SourceSchema,  
        '''+ replace(replace(replace(table_name,'projectionDb_',''),'dbo_',''),'_CT','')+'''									as SourceTable , 
        case  when a.[__$operation] = 4 then ''U''end	as Operation,
        0 as IsProcessing,
                ''{'' +
        '' "Data": {'' + 
        ' + [dbo].[GetTableColumnsAfter](table_name) + '
        ''"},''	+
        ''}'' as json
    from [inserted] a 
    join [inserted] b
        on  a.__$start_lsn			= b.__$start_lsn
        and a.__$seqval			= b.__$seqval
        and b.__$operation	= 3
    where a.__$operation	= 4 
    
'
FROM INFORMATION_SCHEMA.COLUMNS i
where TABLE_SCHEMA = @Schema
and TABLE_NAME = @Table

--Print Convert(ntext, @sql)
EXECUTE sp_executesql @sql

End Try
BEGIN CATCH
    SELECT
        ERROR_NUMBER()		AS ErrorNumber
        ,ERROR_SEVERITY()	AS ErrorSeverity
        ,ERROR_STATE()		AS ErrorState
        ,ERROR_PROCEDURE()	AS ErrorProcedure
        ,ERROR_LINE()		AS ErrorLine
        ,ERROR_MESSAGE()	AS ErrorMessage;
END CATCH;


GO
