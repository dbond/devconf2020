USE [MyOldBankingDB]
GO


CREATE Procedure [cdc].[Process_cdc_change_data_set_for_interval]
    @interval_in_seconds integer
AS
    
    BEGIN TRY

        BEGIN TRAN

            DECLARE @Data TABLE(LSN [binary](10), LSNSV [binary](10), Data varchar(max) NOT NULL)
            Insert into @Data
                select 
                    LSN,
                    LSNSV,
                    'Exec [MyOldBankingDB].[cdc].[Get_cdc_change_data_record] @LSN = '
                    + convert(varchar(max), LSN, 1) + ', @LSNSV = ' +  convert(varchar(max),LSNSV,1)
                    From  [MyOldBankingDB].[cdc].[cdc_change_data]
                    Where IsProcessed = 0
                    And DateDiff(SECOND, ChangeDate, GetDate()) > @interval_in_seconds
                    Order By LSN, LSNSV 

            Insert into [MyOldBankingDB].[queues].[streaming_cdc]
            Select Getdate(), 0, Data from  @Data Order By LSN, LSNSV 

            Update cdc 
            Set IsProcessed = 1
            From [MyOldBankingDB].[cdc].[cdc_change_data] cdc
            Inner Join @Data d
                On cdc.LSN = d.LSN
                And cdc.LSNSV = d.LSNSV

        COMMIT TRAN

    END TRY

    BEGIN CATCH
        ROLLBACK TRAN
    END CATCH
         

GO


