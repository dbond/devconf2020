module.exports = {
    kafka_topic: 'Account',
    kafka_server: 'localhost:9092',
    kafka_group_id: 'demo-reader',
    db_url: 'postgres://app_user:password@localhost:5432/app_db'
  };

  //"postgres://userName:password@serverName/ip:port/nameOfDatabase"

