const config = require('./config');
var reportingRepository = require('./reporting/reportingRepository_v1');
var streamConsumer = require('./streamConsumer/streamConsumer')

  try {
      
    dbRepository = new reportingRepository(config.db_url);
    kafkaConsumer = new streamConsumer(dbRepository, config);
    kafkaConsumer.listenForEvents("AccountCreated", config.kafka_group_id, true, 0);
    
  }
  catch(e) {
    console.log(e);
  }
