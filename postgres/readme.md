# Postgres Environment

To run the postgres and postgrest (REST API for postgres) environments start the docker containers using the docker-compose command.

Navigate to the folder ./docker/postgres off the root of this repository.

Run the command "docker-compose up -d"

    docker-compose up -d

The docker images used will automatically download, after which the docker containers will start.

Note: The download may take a while if you are on a slow data link.

To establish if the containers are running use the command:

    docker ps

Look for the containers postgres_db_1 and postgres_server_1

To set up the Postgres DB follow the steps outlined in the setup_db.md file in the ./scripts folder.

## PostgREST

For documentation on PostgREST follow this link  http://postgrest.org/en/v6.0/
