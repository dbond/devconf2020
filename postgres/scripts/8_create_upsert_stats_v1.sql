--DROP FUNCTION public.update_regsiter_count_v1;

SET search_path = public, pg_catalog;

CREATE OR REPLACE FUNCTION public.update_regsiter_count_v1(year integer, month integer, product integer)
 RETURNS void AS
 $BODY$
    BEGIN
        INSERT INTO public.account_stats_v1 (regyear, regmonth, productId, registerCount)
        VALUES (year, month, product, 1)
        ON CONFLICT (regyear, regmonth, productId)
        DO
            UPDATE
            SET registerCount = public.account_stats_v1.registerCount + 1;

    END;
 $BODY$
 LANGUAGE plpgsql VOLATILE
 COST 100;
 ALTER FUNCTION public.update_regsiter_count_v1(regyear integer, regmonth integer, productId integer)
 OWNER TO app_user;