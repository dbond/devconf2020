
SET search_path = api, pg_catalog;

CREATE VIEW api.MonthlyStatsv1 AS
    SELECT regYear, regMonth, p.productName, registerCount as numberRegistered
    FROM public.account_stats_v1 s
    INNER JOIN public.product p
        ON s.productId = p.productId;


SET search_path = api, pg_catalog;

CREATE VIEW api.MonthlyStatsV2 AS
    SELECT regYear, regMonth, p.productName, c.channelName, registerCount as numberRegistered
    FROM public.account_stats_v2 s
    INNER JOIN public.product p
        ON s.productId = p.productId
    INNER JOIN public.channel c
        ON s.originationChannelId = c.channelId;


