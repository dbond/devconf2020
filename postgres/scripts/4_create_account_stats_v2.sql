--DROP TABLE public.account_stats_v2

SET search_path = public, pg_catalog;

Create table public.account_stats_v2 (
    regyear integer,
    regmonth integer,
    productId integer,
    originationChannelId integer,
    registerCount integer,
    CONSTRAINT stats_date_v2 UNIQUE(regyear, regmonth, productId, originationChannelId)
);