
## Setup server 

Step 1. Connect to the docker container 

    docker exec -it postgres_db_1 /bin/bash
    

Step 2 Connect to psql

    psql -d postgres -U app_user
    sudo docker exec -it postgres_db_1 psql -U postgres

Step 3 Create DB

    createdb app_db

Step 4 Create Schema 
    
    2_createSchema.sql

Step 5 Create projection tables

    3_create_account_stats_v1.sql
    4_create_account_stats_v2.sql

Step 6 Setup Roles

    5_setup_postgrest.sql

Step 7 Setup auth user  

    6_setup_postgrest_auth_user.sql

Step 8 Setup auth user  

    7_setup_app_user.sql

Step 8 Create Upsert

    8_create_upsert_stats_v1.sql
    9_create_upsert_stats_v2.sql

Step 9 Insert master data

    10_insert_master_data.sql

Step 10 Create api views

    11_create_view.sql
