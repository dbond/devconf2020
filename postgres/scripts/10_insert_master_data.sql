--DROP TABLE public.product

SET search_path = public, pg_catalog;

create table public.product
(
	productId int,
	productName varchar(40)
);

create unique index product_productId_uindex
	on public.product (productId);

alter table public.product
	add constraint product_pk
		primary key (productId);

Insert Into product(productId, productName)
    Values (1, 'Product 1');

Insert Into product(productId, productName)
    Values (2, 'Product 2');

Insert Into product(productId, productName)
    Values (3, 'Product 3');

Insert Into product(productId, productName)
    Values (4, 'Product 4');

Insert Into product(productId, productName)
    Values (5, 'Product 5');

--DROP TABLE public.channel

SET search_path = public, pg_catalog;

create table public.channel
(
	channelId int,
	channelName varchar(40)
);

create unique index channel_channelId_uindex
	on public.channel (channelId);

alter table public.channel
	add constraint channel_pk
		primary key (channelId);

Insert Into channel(channelId, channelName)
    Values (1, 'Channel 1');

Insert Into channel(channelId, channelName)
    Values (2, 'Channel 2');

Insert Into channel(channelId, channelName)
    Values (3, 'Channel 3');

Insert Into channel(channelId, channelName)
    Values (4, 'Channel 4');

Insert Into channel(channelId, channelName)
    Values (5, 'Channel 5');




