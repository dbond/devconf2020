--DROP TABLE apublicpi.account_stats_v1

SET search_path = public, pg_catalog;

Create table public.account_stats_v1 (
    regyear integer,
    regmonth integer,
    productId integer,
    registerCount integer,
    CONSTRAINT stats_date_v1 UNIQUE(regyear, regmonth, productId)
);
