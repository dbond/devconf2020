--Setup Roles

create role web_anon nologin;

grant usage on schema api to web_anon;
grant select on api.account_stats to web_anon;