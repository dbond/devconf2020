--DROP FUNCTION public.update_regsiter_count_v2;

SET search_path = public, pg_catalog;

CREATE OR REPLACE FUNCTION public.update_regsiter_count_v2(year integer, month integer, product integer, originationChannel integer)
 RETURNS void AS
 $BODY$
    BEGIN
        INSERT INTO public.account_stats_v2 (regyear, regmonth, productId, originationChannelId, registerCount)
        VALUES (year, month, product, originationChannel, 1)
        ON CONFLICT (regyear, regmonth, productId, originationChannelId)
        DO
            UPDATE
            SET registerCount = public.account_stats_v2.registerCount + 1;

    END;
 $BODY$
 LANGUAGE plpgsql VOLATILE
 COST 100;
 ALTER FUNCTION public.update_regsiter_count_v2(regyear integer, regmonth integer, product integer, originationChannel integer)
 OWNER TO app_user;