# Eventstore

## Docker install

docker pull eventstore/eventstore:release-5.0.3

docker run --name eventstore-node -it -p 2113:2113 -p 1113:1113 eventstore/eventstore


## Subscriptions

### Subscription types
There are three types of subscription patterns, useful in different situations.

#### Volatile subscriptions
This subscription calls a given function for events written after establishing the subscription. They are useful when you need notification of new events with minimal latency, but where it's not necessary to process every event.

For example, if a stream has 100 events in it when a subscriber connects, the subscriber can expect to see event number 101 onwards until the time the subscription is closed or dropped.

#### Catch-up subscriptions
This subscription specifies a starting point, in the form of an event number or transaction file position. You call the function for events from the starting point until the end of the stream, and then for subsequently written events.

For example, if you specify a starting point of 50 when a stream has 100 events, the subscriber can expect to see events 51 through 100, and then any events are subsequently written until you drop or close the subscription.

#### Persistent subscriptions
NOTE
Persistent subscriptions exist in version 3.2.0 and above of Event Store.

In contrast to volatile and Catch-up types persistent subscriptions are not dropped when connection is closed. Moreover, this subscription type supports the "competing consumers" messaging pattern and are useful when you need to distribute messages to many workers. Event Store saves the subscription state server-side and allows for at-least-once delivery guarantees across multiple consumers on the same stream. It is possible to have many groups of consumers compete on the same stream, with each group getting an at-least-once guarantee.


### Event Store Connection
The EventStoreConnection class maintains a full-duplex connection between the client and the Event Store server. EventStoreConnection is thread-safe, and we recommend that you create one node per application.

Event Store handles all connections asynchronously, returning either a Task or a Task<T>. If you need to execute synchronously, call .Wait() on the asynchronous version.

### Meta Data
https://eventstore.com/docs/server/metadata-and-reserved-names/index.html
Every stream in Event Store has metadata stream associated with it, prefixed by $$, so the metadata stream from a stream called foo is $$foo. Event Store allows you to change some values in the metadata, and you can write your own data into stream metadata that you can refer to in your code.

This information is not part of the actual event but is metadata associated with the event. Event Store stores stream metadata as JSON, and you can access it over the HTTP APIs.

You can add user-specified metadata via the SetCustomMetadata overloads. Some examples of good uses of user-specified metadata are:

which adapter is responsible for populating a stream.
which projection caused a stream to be created.
a correlation ID of some business process.

If you enabled enabled security, reading metadata may require that you pass credentials. By default it is only allowed for admins though you can change this via default ACLs. If you do not pass credentials and they are required you will receive an 

### Event Scavengers

When you delete events or streams in Event Store, they aren't removed immediately. To permanently delete these events you need to run a 'scavenge' on your database.

A scavenge reclaims disk space by rewriting your database chunks, minus the events to delete, and then deleting the old chunks. Scavenges only affect completed chunks, so deleted events in the current chunk are still there after you run a scavenge.

After processing the chunks, the operation updates the chunk indexes using a merge sort algorithm, skipping events whose data is no longer available.

Scavenges are not run automatically by Event Store. We recommendation that you set up a scheduled task, for example using cron or Windows Scheduler, to trigger a scavenge as often as you need.

curl -i -d {} -X POST http://localhost:2113/admin/scavenge -u "admin:changeit"


https://eventstore.com/docs/server/scavenging/index.html?tabs=tabid-8

### Writing a client

.Net Connector
https://www.nuget.org/packages/EventStore.Client.Grpc/

Install-Package EventStore.Client.Grpc -Version 6.0.0-preview2

dotnet add package EventStore.Client.Grpc --version 6.0.0-preview2

dotnet add package EventStore.Client

https://eventstore.com/docs/getting-started/?tabs=tabid-1%2Ctabid-dotnet-client%2Ctabid-dotnet-client-connect%2Ctabid-4

### Event Projections

https://eventstore.com/docs/projections/user-defined-projections/index.html

Projections is an Event Store subsystem that lets you write new events or link existing events to streams in a reactive manner.

Projections are good at solving one specific query type, a category known as 'temporal correlation queries'. This query type is common in business systems and few can execute these queries well.

https://eventstore.com/docs/getting-started/projections/index.html?tabs=tabid-1%2Ctabid-4%2Ctabid-http-api%2Ctabid-create-proj-bash%2Ctabid-8%2Ctabid-update-proj-http%2Ctabid-reset-http%2Ctabid-read-stream-http%2Ctabid-update-proj-config-http%2Ctabid-read-projection-events-renamed-http%2Ctabid-enablebycategory-http%2Ctabid-projections-count-per-stream-http%2Ctabid-read-partition-http

Event Store has a built-in $by_category projection that lets you select events from a particular list of streams. Enable this projection with the following command.

curl -i -d{} http://localhost:2113/projection/%24by_category/command/enable -u admin:changeit

The projection links events from existing streams to new streams by splitting the stream name by a separator. You can configure the projection to specify the position of where to split the stream id and provide a separator.

By default, the category splits the stream id by a dash. The category is the first string.

You want to define a projection that produces a count per stream for a category, but the state needs to be per stream. To do so, use $by_category and its fromCategory API method.

Below is the projection, you can download the file here:

fromCategory('shoppingCart')
.foreachStream()
.when({
    $init: function(){
        return {
            count: 0
        }
    },
    ItemAdded: function(s,e){
        s.count += 1;
    }
})