﻿using System.Configuration;

namespace LegacyEventStreaming.Mssql.Config
{
    public class MssqlConfig : IDbConfig
    {
        public string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["DBCONNECTION_MyOldBankingApp"].ToString(); }
        }

        public string BatchReadSize
        {
            get { return ConfigurationManager.AppSettings["dbBatchReadSize"]; }
        }
    }
}