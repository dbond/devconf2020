﻿using System;

namespace LegacyEventStreaming.Mssql.ChangeData
{
    public interface IChangeDataRecord
    {
        long Id { get; set; }
        DateTime ChangeDate { get; set; }
        byte[] LSN { get; set; }
        byte[] LSNSV { get; set; }
        string SourceDatabase { get; set; }
        string SourceSchema { get; set; }
        string SourceTable { get; set; }
        string Operation { get; set; }
        string Json { get; set; }
    }
}