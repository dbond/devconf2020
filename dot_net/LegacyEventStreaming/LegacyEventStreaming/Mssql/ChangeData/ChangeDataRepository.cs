﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using LegacyEventStreaming.Mssql.Config;

namespace LegacyEventStreaming.Mssql.ChangeData
{
    public class ChangeDataRepository : IChangeDataRepository
    {
        
        private IDbConfig _configuration;

        public ChangeDataRepository(IDbConfig configuration)
        {
            _configuration = configuration;
        }
        
        public ChangeDataRecord ReadNextChangeDataRecord()
        {
            ChangeDataRecord changeDataRecord = null;
            try
            {
                using (var sqlConnection = new System.Data.SqlClient.SqlConnection(_configuration.ConnectionString))
                {
                    sqlConnection.Open();
                    changeDataRecord = sqlConnection.QuerySingle<ChangeDataRecord>(
                        @"select top 1 Id, LSN, LSNSV, ChangeDate, SourceDatabase, SourceSchema, SourceTable, Operation, IsProcessed, json from cdc.cdc_change_data where IsProcessed = 0");

                    return changeDataRecord;
                }
            }
            catch(Exception exception)
            {
                Console.WriteLine(exception.Message);
            }

            return changeDataRecord;
        }

        public ChangeDataRecord ReadNextQueuedChangeDataRecord()
        {
            ChangeDataRecord changeDataRecord = null;

            try
            {
                Console.WriteLine("Connect to DB");
                Console.WriteLine(_configuration.ConnectionString);
                using (var sqlConnection = new SqlConnection(_configuration.ConnectionString))
                {
                    Console.WriteLine("Open connection to DB");
                    sqlConnection.Open();
                    Console.WriteLine("Run Query to DB");
                    List<QueuedChangeDataRecord> queuedChangeDataRecords = sqlConnection.Query<QueuedChangeDataRecord>(
                            @"Select top 1 Id, QueueDate, IsProcessing, Data From queues.streaming_cdc Where IsProcessing = 0 Order By Id ASC")
                        .ToList();
                    
                    Console.WriteLine(queuedChangeDataRecords);
                    if (queuedChangeDataRecords != null)
                    {
                        if (queuedChangeDataRecords.Count == 1)
                        {
                            string newQuery = queuedChangeDataRecords[0].Data;
                            long queueId = queuedChangeDataRecords[0].Id;

                            return ReadChangeDataRecord(sqlConnection, newQuery, queueId);

                        }
                    }
                    
                }
            }
            catch(Exception exception)
            { 
                Console.WriteLine(exception.Message);
            }
            return changeDataRecord;
        }

        public List<ChangeDataRecord> ReadNextBatchOfQueuedChangeDataRecords()
        {
            List<ChangeDataRecord> changeDataRecords = new List<ChangeDataRecord>();
            using (var sqlConnection = new SqlConnection(_configuration.ConnectionString))
            {
                sqlConnection.Open();
                List<QueuedChangeDataRecord> queuedChangeDataRecords = sqlConnection.Query<QueuedChangeDataRecord>(
                        @"Select top @batchCount Id, QueueDate, IsProcessing, Data From queues.streaming_cdc Where IsProcessing = 0 Order By Id ASC", 
                        new { batchCount = _configuration.BatchReadSize  })
                    .ToList();

                if (queuedChangeDataRecords.Count > 0)
                {
                    foreach (QueuedChangeDataRecord queuedChange in queuedChangeDataRecords)
                    {
                        string newQuery = queuedChange.Data;
                        long queueId = queuedChange.Id;
                        
                        var changeDataRecord = ReadChangeDataRecord(sqlConnection, newQuery, queueId);
                        changeDataRecords.Add(changeDataRecord);
                        
                    }
                    
                }

                return changeDataRecords;
            }
        }

        private ChangeDataRecord ReadChangeDataRecord(SqlConnection sqlConnection, string newQuery, long queueId)
        {
            ChangeDataRecord changeDataRecord = new ChangeDataRecord();

            List<ChangeDataRecord> dataChanges = sqlConnection.Query<ChangeDataRecord>(newQuery).ToList();

            if (dataChanges.Count == 1)
            {
                changeDataRecord = dataChanges[0];
            }
            else if (dataChanges.Count > 1)
            {
                changeDataRecord = dataChanges.FirstOrDefault(x => x.Operation.Equals("I"));
                changeDataRecord.Operation = "U";
            }
            
            changeDataRecord.QueueId = queueId;

            return changeDataRecord;
        }

        public void UpdateChangeDataRecordAsProcessed(long id)
        {
            using (var sqlConnection = new SqlConnection(_configuration.ConnectionString))
            {
                sqlConnection.Open();
                sqlConnection.Execute(@"Update cdc.cdc_change_data Set IsProcessed = 1 Where Id = @Id", new {Id = id});
            }
        }
        
        public void UpdateQueuedChangeDataRecordAsProcessed(long id)
        {
            using (var sqlConnection = new SqlConnection(_configuration.ConnectionString))
            {
                sqlConnection.Open();
                sqlConnection.Execute(@"Update queues.streaming_cdc Set IsProcessing = 1 Where Id = @Id", new {Id = id});
            }
        }

        public void DeleteProcessedQueuedChangeDataRecord(long id)
        {
            using (var sqlConnection = new SqlConnection(_configuration.ConnectionString))
            {
                sqlConnection.Open();
                sqlConnection.Execute(@"Delete From queues.streaming_cdc Where Id = @Id", new {Id = id});
            }
        }

    }
}