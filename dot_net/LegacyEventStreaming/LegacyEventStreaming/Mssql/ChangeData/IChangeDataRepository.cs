﻿namespace LegacyEventStreaming.Mssql.ChangeData
{
    public interface IChangeDataRepository
    {
        ChangeDataRecord ReadNextChangeDataRecord();
        ChangeDataRecord ReadNextQueuedChangeDataRecord();
        void UpdateChangeDataRecordAsProcessed(long id);
        void UpdateQueuedChangeDataRecordAsProcessed(long id);
        void DeleteProcessedQueuedChangeDataRecord(long id);
    }
}