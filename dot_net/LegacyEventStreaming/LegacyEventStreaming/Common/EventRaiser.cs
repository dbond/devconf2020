﻿using System;
using System.Collections.Generic;
using LegacyEventStreaming.Common.EventStreaming;
using LegacyEventStreaming.Mssql.ChangeData;
using LegacyEventStreaming.Streaming.EventHandlers;

namespace LegacyEventStreaming.Common
{
    public class EventRaiser
    {

        private IChangeDataRepository _dataRepository;
        private IEventHandlers _eventHandlers;
        
        public EventRaiser(IChangeDataRepository dataRepository, IEventHandlers eventHandlers)
        {
            _dataRepository = dataRepository;
            _eventHandlers = eventHandlers;
        }

        public void RaiseEvent()
        {
            Console.WriteLine("Reading Next Data Change Record");
            var changeData = _dataRepository.ReadNextQueuedChangeDataRecord();

            if (changeData != null)
            {
                //get event handler
                Console.WriteLine("Get Event Handlers");
                List<IEventHandler> eventHandlers = _eventHandlers.GetEventHandlers(changeData.SourceTable);

                if (eventHandlers != null)
                {
                    Console.WriteLine("Processing : " + changeData.QueueId);
                    _dataRepository.UpdateQueuedChangeDataRecordAsProcessed(changeData.QueueId);
                    foreach (IEventHandler eventHandler in eventHandlers)
                    {
                        Console.WriteLine("Raising events for : " + changeData.QueueId);
                        eventHandler.HandleEvent(changeData);
                    }
                    Console.WriteLine("Delete queued item for : " + changeData.QueueId);
                    _dataRepository.DeleteProcessedQueuedChangeDataRecord(changeData.QueueId);  
                    
                }
            }
        }
    }
}