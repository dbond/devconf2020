﻿using LegacyEventStreaming.Common.EventStreaming;
using LegacyEventStreaming.Mssql.ChangeData;

namespace LegacyEventStreaming.Common.EventBuilder
{
    public interface  IEventBuilder
    {
        StreamingEvent CreateEvent(IChangeDataRecord changeDataRecord);
    }
}