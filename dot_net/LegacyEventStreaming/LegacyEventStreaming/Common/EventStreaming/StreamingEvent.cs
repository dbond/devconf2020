﻿namespace LegacyEventStreaming.Common.EventStreaming
{
    public class StreamingEvent
    {
        public string Key { get; set; }
        public string Value { get; set; }
        
        public string EventType { get; set; }
    }
}