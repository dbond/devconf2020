﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace LegacyEventStreaming.Common.Serialisation
{
    
    public class LegacyEventConverter<T> : CustomCreationConverter<T>
    {
        public override T Create(Type objectType)
        {
            return (T) Activator.CreateInstance(typeof(T));
        }
        
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jObject = JObject.Load(reader);
            string data = jObject["Data"].ToString();
            return JsonConvert.DeserializeObject<T>(data);
        }
    }
}