﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;
using LegacyEventStreaming.Common;
using LegacyEventStreaming.Common.EventStreaming;
using LegacyEventStreaming.Mssql.ChangeData;
using LegacyEventStreaming.Mssql.Config;
using LegacyEventStreaming.Streaming.EventHandlers;
using LegacyEventStreaming.Streaming.Kafka;

namespace LegacyEventStreaming
{
    class Program
    {
        public static void Main(string[] args)
        {

            Console.WriteLine("Load DB Configuration");
            IDbConfig config = new MssqlConfig();
            
            Console.WriteLine("Initialise Change Data Repository");
            ChangeDataRepository repository = new ChangeDataRepository(config);
            
            Console.WriteLine("Load Event Handlers");
            IEventHandlers eventHandlers = LoadEventHandler(args);
            
            Console.WriteLine("Initialise Event Raiser");
            EventRaiser eventRaiser = new EventRaiser(repository, eventHandlers);
            
            Console.WriteLine("Setup Application Termination Handler");
            var cancellationToken = ApplicationManager.SetupCancellationToken();
            
            while (!cancellationToken.IsCancellationRequested)
            {
                eventRaiser.RaiseEvent();
                Thread.Sleep(1000);
            }
            
            Console.WriteLine("Done -> Terminating");
            
        }

        private static IEventHandlers LoadEventHandler(string[] args)
        {
            //if (args.Length == 0) { return new KafkaEventHandlers(); }
            if (args.Length == 0) { return new EventStoreEventHandlers(); }
            if (args[0].ToLower().Equals("kafka")) { return new KafkaEventHandlers(); }
            if (args[0].ToLower().Equals("eventstore")) { return new EventStoreEventHandlers(); }
            return null;
        }
    }
}