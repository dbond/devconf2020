﻿using System.Collections.Generic;
using LegacyEventStreaming.Common;
using LegacyEventStreaming.Common.EventStreaming;

namespace LegacyEventStreaming.Streaming.EventHandlers
{
    public interface IEventHandlers
    {
        List<IEventHandler> GetEventHandlers(string eventName);
    }
}