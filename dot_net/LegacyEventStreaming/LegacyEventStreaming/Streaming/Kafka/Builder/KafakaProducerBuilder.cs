﻿using Confluent.Kafka;

namespace LegacyEventStreaming.Streaming.Kafka.Builder
{
    public class KafakaProducerBuilder
    {

        private KafkaConfigBuilder _builder;
        public KafakaProducerBuilder()
        {
            _builder = new KafkaConfigBuilder();
        }
        
        public IProducer<string, string> Build()
        {
            var config = _builder.CreateConfig()
                            .WithBootstrapServer()
                            .Build();
            
            return new ProducerBuilder<string, string>(config).Build();
        }

    }
}