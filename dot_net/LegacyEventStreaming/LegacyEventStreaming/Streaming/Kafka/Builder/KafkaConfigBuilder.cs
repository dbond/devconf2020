﻿using System.Configuration;
using Confluent.Kafka;

namespace LegacyEventStreaming.Streaming.Kafka.Builder
{
    public class KafkaConfigBuilder
    {
        
        private ProducerConfig config;
        
        public KafkaConfigBuilder CreateConfig()
        {
            config = new ProducerConfig();
            return this;
        }

        public KafkaConfigBuilder WithBootstrapServer()
        {
            config.BootstrapServers = ConfigurationManager.AppSettings["kafkaBrokerList"];
            return this;
        }

        public ProducerConfig Build()
        {
            return config;
        }
        
    }
}