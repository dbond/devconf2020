﻿using System;
using System.Text;
using System.Threading.Tasks;
using EventStore.ClientAPI;
using LegacyEventStreaming.Common.EventStreaming;
using LegacyEventStreaming.Streaming.EventStore.Builder;

namespace LegacyEventStreaming.Streaming.EventStore
{
    public class EventStoreEventProducer : IEventProducer
    {

        private IEventStoreConnection _connection;
        public EventStoreEventProducer()
        {
            EventStoreConfigBuilder configBuilder = new EventStoreConfigBuilder();
        }
        public async Task<bool> HandleEvent(string stream, StreamingEvent streamingEvent)
        {
            IEventStoreConnection connection = CreateConnection();
            
            try
            {
                var eventData = Encoding.UTF8.GetBytes(streamingEvent.Value);
                var eventMetaData = Encoding.UTF8.GetBytes("{}");
                var eventStoreEvent = new EventData(Guid.NewGuid(), streamingEvent.EventType, true, eventData, eventMetaData);

                var writeResult = await _connection.AppendToStreamAsync(stream, ExpectedVersion.Any, eventStoreEvent);

                Console.WriteLine($"delivered to: { writeResult.LogPosition }");

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine($"failed to deliver message: {e.Message}");
                return false;
            }
        }

        private IEventStoreConnection CreateConnection()
        {
            if (_connection == null)
            {
                string connectionString = new EventStoreConfigBuilder()
                                                    .WithConfigHost()
                                                    .WithConfigUser()
                                                    .WithConfigPassword()
                                                    .Build();
                
                _connection = EventStoreConnection.Create(new Uri(connectionString));
                _connection.ConnectAsync().Wait();
            }

            return _connection;
        }
    }
}