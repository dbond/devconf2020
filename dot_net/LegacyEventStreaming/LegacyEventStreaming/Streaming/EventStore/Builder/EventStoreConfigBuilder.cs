﻿using System;
using System.Configuration;

namespace LegacyEventStreaming.Streaming.EventStore.Builder
{
    public class EventStoreConfigBuilder
    {
        private string _protocol = "tcp";
        private string _user = "admin";
        private string _password = "changeit";
        private string _host = "localhost";
        private string _port = "1113";

        public EventStoreConfigBuilder WithConfigUser()
        {
            _user = ConfigurationManager.AppSettings["eventStoreUserName"];
            return this;
        }
        
        public EventStoreConfigBuilder WithConfigPassword()
        {
            _password = ConfigurationManager.AppSettings["eventStorePassword"];
            return this;
        }
        
        public EventStoreConfigBuilder WithConfigHost()
        {
            _host = ConfigurationManager.AppSettings["eventStoreHost"];
            return this;
        }
        
        public string Build()
        {
            return $"{_protocol}://{_user}:{_password}@{_host}:{_port}";
        }
    }
}