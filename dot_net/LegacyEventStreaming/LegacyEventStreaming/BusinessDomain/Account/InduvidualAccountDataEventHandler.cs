﻿using System;
using System.Threading.Tasks;
using LegacyEventStreaming.Common.EventBuilder;
using LegacyEventStreaming.Common.EventStreaming;
using LegacyEventStreaming.Common.Serialisation;
using LegacyEventStreaming.Mssql.ChangeData;
using LegacyEventStreaming.Streaming;
using Newtonsoft.Json;

namespace LegacyEventStreaming.BusinessDomain.Account
{
    public class InduvidualAccountDataEventHandler : IEventHandler
    {
        private IEventProducer _eventProducer;
        private IEventBuilder _eventBuilder;
        
        public InduvidualAccountDataEventHandler(IEventProducer eventProducer, IEventBuilder eventBuilder)
        {
            _eventProducer = eventProducer;
            _eventBuilder = eventBuilder;
        }
        
        public bool HandleEvent(IChangeDataRecord changeDataRecord)
        {
            StreamingEvent streamingEvent = _eventBuilder.CreateEvent(changeDataRecord);
            
            Account legacyEvent = JsonConvert.DeserializeObject<Account>(changeDataRecord.Json, new LegacyEventConverter<Account>());
            
            string stream = $"Account-{legacyEvent.AccountId.ToString()}";
            Task<bool> sendTask = _eventProducer.HandleEvent(stream, streamingEvent);
            sendTask.Wait();
            return sendTask.Result;

        }
    }
}