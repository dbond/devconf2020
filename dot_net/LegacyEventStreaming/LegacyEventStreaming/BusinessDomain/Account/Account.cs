﻿using System;
using LegacyEventStreaming.Common;
using LegacyEventStreaming.Common.EventBuilder;

namespace LegacyEventStreaming.BusinessDomain.Account
{
    public class Account : ILegacyEvent
    {
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public int AccountStatusId { get; set; }
        public DateTime OpenDate  { get; set; }
        public DateTime? ClosedDate { get; set; }
        public int OriginationChannelId { get; set; }
        public int ProductId { get; set; }
        public int InvestmentVehicleId { get; set; }
        public string Action { get; set; }
    }
}