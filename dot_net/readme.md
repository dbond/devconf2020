# Dot Net Connector

This section of the prototype contains the dot net core code for the Ambassador (Proxy), which reads the event stream from MSSQL and writes it to either Kafka or Event Store.

The application has a simple repository which connects with MSSQL to read and manage events.

Events read from the MSSQL are either dispatched to Kafka or Event Store depending on the configuration.

Events generated from MSSQL have the following structure:

    {
        "Data": 
        { 
            "AccountId":"9", 
            "AccountName":"Investment 1", 
            "AccountStatusId":"1", 
            "OpenDate":"Jan 10 2020 12:00AM", 
            "ClosedDate":"", 
            "OriginationChannelId":"1", 
            "ProductId":"1", 
            "InvestmentVehicleId":"100"
        }
    }

Note: This is a data change event for the Account table

Events are manipulated by the ambassador application to convert the events into the correct format for Kafka and Event Store.

The events are then dispatched to either Kafka or Event Store using one of three dispatch strategies:

1. Dispatch to an account created event stream (only if the is an inserted event)
2. Dispatch to a raw account event stream, first add the action that created the event to the event (either I (inserted), U (updated), D (deleted))
3. Dispatch to a unique event steam for each Account, the stream name will be Account-{accountId}. This strategy is helpful to keep all events for a particular account on one stream. This approach is used when using you event stream as an event database, the stream can be used to populate your DDD Aggregate, instead of using a relational db. Replay in the events will build your Aggregate.

This code was designed to run in a docker container. The Dockerfile used to build this container is included.

Please note that none of this code is production ready.

Parts of the code are taken from a production system, but since this is for demonstration the production robustness required from a solution like this has been removed.

The Docker Compose files for Kafka and Event Store need to be running to work with this part of teh prototype.

## Sample Kafka Code

Helpful links

Kafka Dot Net Examples

https://docs.confluent.io/current/clients/dotnet.html
https://cwiki.apache.org/confluence/display/KAFKA/Clients#Clients-.NET
https://github.com/confluentinc/confluent-kafka-dotnet
https://github.com/confluentinc/confluent-kafka-dotnet/blob/master/examples/Producer/Program.cs